# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.1.0

- minor: Support using latest JFrog CLI. Upgrade default version to 2.56.1
- patch: Fix issues in release flow

## 2.0.0

- major: Promote JFrog CLI to v2

## 1.0.0

- major: Initial Release
